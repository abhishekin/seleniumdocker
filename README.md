# README #

## Versions Requirement

- Apache Maven 3.8.1
- Java 11 
- Chromedriver 90.0

## How to Run ?

### Intellij

1. Import as Maven Project
2. Right click on POM and select Update Maven 
3. Right click on TC01_Signup and click on "Run TC01_Signup

### Terminal

From project directory ,run:

`mvn clean test -DsuiteXmlFile=RunAllTests.xml`

#### Note

* I made tests compatible to run on any OS, although I only tried on Windows.
* TestUtils.getNewEmail is generating Fake email ids for Test, no need to update manually

Feel free to reachout to me incase of any issues.