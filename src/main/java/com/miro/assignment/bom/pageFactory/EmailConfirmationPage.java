package com.miro.assignment.bom.pageFactory;

import com.miro.assignment.utils.TestUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class EmailConfirmationPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */
    WebDriver driver;

    @FindBy(xpath = "//*[@class='signup__title-form']")
    WebElement signupTitleForm;

    @FindBy(xpath = "//*[@class='signup__subtitle-form']/strong")
    WebElement email;

    @FindBy(name = "code")
    WebElement codeBox;

    @FindBy(xpath = "//a[@href='/get-new-code/']")
    WebElement signupFooter;

    @FindBy(id = "error-general")
    WebElement errorGeneral;

    @FindBy(id = "error-attemptsExceeded")
    WebElement errorExceeded;


    public EmailConfirmationPage(WebDriver driver) {
        this.driver = driver;
        //This initElements method will create  all WebElements
        PageFactory.initElements(driver, this);
    }

    public boolean verifySignupTitleForm(WebDriver driver, String titleformtext) {
        TestUtils.waitForElementToBeVisible(driver, signupTitleForm);
        return TestUtils.verifyText(signupTitleForm, titleformtext);
    }

    public boolean verifyEmail(WebDriver driver, String emailtext) {
        TestUtils.waitForElementToBeVisible(driver, email);
        return TestUtils.verifyText(email, emailtext);
    }

    public boolean enterCode(WebDriver driver, String code) {
        TestUtils.waitForElementToBeClickable(driver, codeBox);
        return TestUtils.enterValue(codeBox, code);
    }

    public boolean verifyIncorrectError(WebDriver driver, String errorText) {
        TestUtils.waitForElementToBeVisible(driver, errorGeneral);
        return TestUtils.verifyText(errorGeneral, errorText);
    }

    public boolean verifyExceedError(WebDriver driver, String exceederrortext) {
        TestUtils.waitForElementToBeVisible(driver, errorExceeded);
        return TestUtils.verifyText(errorExceeded, exceederrortext);
    }

    public boolean clickSendCodeAgain(WebDriver driver) {
        TestUtils.waitForElementToBeClickable(driver, signupFooter);
        return TestUtils.click("Send code again", signupFooter);
    }
}
