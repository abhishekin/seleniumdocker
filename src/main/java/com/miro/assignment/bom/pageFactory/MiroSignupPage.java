package com.miro.assignment.bom.pageFactory;

import com.miro.assignment.utils.TestUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MiroSignupPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */
    WebDriver driver;

    @FindBy(id = "name")
    WebElement name;

    @FindBy(id = "email")
    WebElement workEmail;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(xpath = "//*[@for='signup-terms']")
    WebElement miroTerms;

    @FindBy(xpath = "//*[@for='signup-subscribe']")
    WebElement miroNewsSubscribe;

    @FindBy(xpath = "//*[@class='signup__submit']")
    WebElement getStartedNow;

    @FindBy(xpath = "//*[@class='signup__error-item']")
    WebElement termsErrorMessage;

    @FindBy(xpath = "//*[@data-autotest-id='please-enter-your-password-1']")
    WebElement emptyPasswordErrorMessage;

    @FindBy(xpath = "//*[@id='onetrust-accept-btn-handler']")
    WebElement acceptCookies;

    @FindBy(xpath = "//*[@data-autotest-id=\"mr-error-signup.error.emptyname-1\"]")
    WebElement nameErrorMessage;

    @FindBy(xpath = "//*[@data-autotest-id=\"mr-error-signup.error.emptyemail-1\"]")
    WebElement emailErrorMessage;

    @FindBy(xpath = "//*[contains(@class,\"signup__input-hint-text signup\")]")
    WebElement passwordInputHint;


    public MiroSignupPage(WebDriver driver) {
        this.driver = driver;
        //This initElements method will create  all WebElements
        PageFactory.initElements(driver, this);
    }

    public boolean enterName(WebDriver driver, String Name) {
        TestUtils.waitForElementToBeClickable(driver, name);
        return TestUtils.enterValue(name, Name);
    }

    public boolean enterEmail(WebDriver driver, String email) {
        TestUtils.waitForElementToBeClickable(driver, workEmail);
        return TestUtils.enterValue(workEmail, email);
    }

    public boolean enterPassword(WebDriver driver, String Password) {
        TestUtils.waitForElementToBeClickable(driver, password);
        return TestUtils.enterValue(password, Password);
    }

    public boolean checkMiroTerms(WebDriver driver) {
        TestUtils.waitForElementToBeClickable(driver, miroTerms);
        return TestUtils.click("Miro Terms", miroTerms);
    }

    public boolean clickGetStartedNow(WebDriver driver) {
        TestUtils.waitForElementToBeClickable(driver, getStartedNow);
        return TestUtils.click("GetStartedNow", getStartedNow);
    }

    public boolean clickAcceptCookies(WebDriver driver) {
        TestUtils.waitForElementToBeClickable(driver, acceptCookies);
        return TestUtils.click("Accept Cookies", acceptCookies);
    }

    public boolean checkMiroSubscribe(WebDriver driver) {
        TestUtils.waitForElementToBeVisible(driver, miroNewsSubscribe);
        return TestUtils.click("Miro Subscribe", miroNewsSubscribe);
    }

    public boolean verifyTermsErrorMessage(WebDriver driver, String termerrortext) {
        TestUtils.waitForElementToBeVisible(driver, termsErrorMessage);
        return TestUtils.verifyText(termsErrorMessage, termerrortext);
    }

    public boolean verifyEmptyPasswordErrorMessage(WebDriver driver, String exceederrortext) {
        TestUtils.waitForElementToBeVisible(driver, emptyPasswordErrorMessage);
        return TestUtils.verifyText(emptyPasswordErrorMessage, exceederrortext);
    }

    public boolean verifyNameErrorMessage(WebDriver driver, String nameerrortext) {
        TestUtils.waitForElementToBeVisible(driver, nameErrorMessage);
        return TestUtils.verifyText(nameErrorMessage, nameerrortext);
    }

    public boolean verifyEmailErrorMessage(WebDriver driver, String emailerrortext) {
        TestUtils.waitForElementToBeVisible(driver, emailErrorMessage);
        return TestUtils.verifyText(emailErrorMessage, emailerrortext);
    }

    public boolean verifyPasswordInputHint(WebDriver driver, String passwordhinttext) {
        TestUtils.waitForElementToBeVisible(driver, passwordInputHint);
        return TestUtils.verifyText(passwordInputHint, passwordhinttext);
    }

}
