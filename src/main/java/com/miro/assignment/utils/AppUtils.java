package com.miro.assignment.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AppUtils {
    public static String getNewEmail(String domain, int length) {
        return RandomStringUtils.random(length, "abcdefghijklmnopqrstuvwxyz1234567890") + "@" + domain;
    }

    public static void runTerminalCommand(String command,String logText)
    {
        Process p;
        try {
            String os = System.getProperty("os.name").toLowerCase();
            System.out.println("linsssssssssssssssssssssssssssse" + os);
            if (os.contains("windows")) {
                System.out.println("Insideee windowssssssssssss" );
                String path = System.getProperty("user.dir");
                ProcessBuilder builder = new ProcessBuilder(
                        "cmd.exe", "/c", "cd \"" + path + "\" && " + command);
                builder.redirectErrorStream(true);
                p = builder.start();
            }
            else
            {
                p = Runtime.getRuntime().exec(command);
                System.out.println("Insideee Linuxxxxxxxxx" );
            }
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            if(logText != null && !logText.trim().isEmpty()) {
                while (true) {
                    line = r.readLine();
                    if (line.contains(logText)) {
                        Thread.sleep(5000);
                        System.out.println(line);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
