package com.miro.assignment.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.time.Duration;

public class TestUtils {

    public static boolean enterValue(WebElement element, String strValue) {
        boolean isValueEntered = false;
        element.clear();
        element.sendKeys(strValue);
        if (element.getAttribute("value").equals(strValue)) {
            isValueEntered = true;
            Reporter.log("PASS, Entered value '" + strValue + "' in textbox", true);
        } else {
            Reporter.log("FAIL, Unable to enter value '" + strValue + "' in textbox", true);
        }
        return isValueEntered;
    }


    public static boolean click(String elementname, WebElement element) {
        boolean isClicked = false;
        if (element.isDisplayed()) {
            element.click();
            isClicked = true;
            Reporter.log("PASS, Clicked element '" + elementname + "'", true);
        } else {
            Reporter.log("FAIL, Unable to click element '" + elementname + "'", true);
        }
        return isClicked;
    }

    public static boolean verifyText(WebElement element, String strValue) {
        boolean isTextValid = false;
        String str = element.getText();
        if (str.equals(strValue)) {
            isTextValid = true;
            Reporter.log("PASS, Element contains text: '" + str + "", true);
        } else {
            Reporter.log("FAIL, Element donot contains text: '" + strValue + "", true);
        }
        return isTextValid;
    }


    public static void waitForElementToBeVisible(WebDriver driver, WebElement element) {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementToBeClickable(WebDriver driver, WebElement element) {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(element));
    }

}

