package com.miro.assignment;

import com.miro.assignment.bom.pageFactory.EmailConfirmationPage;
import com.miro.assignment.bom.pageFactory.MiroSignupPage;
import com.miro.assignment.config.TestConfiguration;
import com.miro.assignment.utils.AppUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TC01_SignUp extends  TestConfiguration {

    WebDriver driver;
    MiroSignupPage miroSignupPage;
    EmailConfirmationPage emailConfirmationPage;
    String signupurl="https://miro.com/signup/";

    @BeforeTest
    public void setup() throws MalformedURLException {
        //driver = new TestConfiguration().getDriver();
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setBrowserName(BrowserType.CHROME);
        //driver = new ChromeDriver();
        driver = new RemoteWebDriver(new URL("http://localhost:4444"),cap);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(priority=1)
    public void test_validateSignup() {
        System.out.println("Test Case One in " + getClass().getSimpleName()
                + " with Thread Id:- " + Thread.currentThread().getId());
        String email= AppUtils.getNewEmail("noemail.com",10);
        String password="Password!@#123";
        driver.get(signupurl);
        //Create miroSignupPage & emailConfirmationPage objects
        miroSignupPage = new MiroSignupPage(driver);
        emailConfirmationPage = new EmailConfirmationPage(driver);
        Reporter.log("Running Tests: test_validateSignup" , true);
        Assert.assertTrue(miroSignupPage.enterName(driver,"TestUser"));
        Assert.assertTrue(miroSignupPage.enterEmail(driver,email));
        Assert.assertTrue(miroSignupPage.enterPassword(driver,password));
        Assert.assertTrue(miroSignupPage.verifyPasswordInputHint(driver,"Good password"));
        Assert.assertTrue(miroSignupPage.checkMiroTerms(driver));
        Assert.assertTrue(miroSignupPage.checkMiroSubscribe(driver));
        Assert.assertTrue(miroSignupPage.clickGetStartedNow(driver));
        Assert.assertTrue(emailConfirmationPage.verifySignupTitleForm(driver,"Check your email"));
        Assert.assertTrue(emailConfirmationPage.verifyEmail(driver,email));
        Assert.assertTrue(emailConfirmationPage.clickSendCodeAgain(driver));
        Assert.assertTrue(emailConfirmationPage.verifySignupTitleForm(driver,"Check your email"));
        Assert.assertTrue(emailConfirmationPage.verifyEmail(driver,email));
        Reporter.log("DONE,Test Completed: test_validateSignup" , true);
    }

    @AfterTest
    public void teardown(){
        driver.close();
    }
}

