package com.miro.assignment.config;

import com.miro.assignment.utils.AppUtils;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class TestConfiguration {

    @BeforeSuite
    public void startContainer()
    {
        System.out.println("Before Suite");
        //AppUtils.runTerminalCommand("docker-compose up","Starting Selenium Grid Node");
        System.out.println("Before Suite Done");
    }


    @AfterSuite
    public void stopContainer() throws InterruptedException {
        System.out.println("After Suite");
        //AppUtils.runTerminalCommand("docker-compose down","");
        Thread.sleep(5000);
    }

}
